#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
L'objectif de ce script est de collecter en SFTP des fichiers journalisés.
"""

# Return codes :
#   0 Fin d'exécution normale
#   1 Le daemon est déjà en cours d'exécution
#   2 Le fichier de configuration n'existe pas
#   3 La configuration est invalide
#   4 Le home du daemon n'a pas pu être créé
#   5 La connection à un serveur distant a échoué

from datetime import datetime
from logging.handlers import SysLogHandler
import errno
import hashlib
import logging
import os
import pexpect
import re
import signal
import sys
import time
import yaml


class CollectorFileGroup:
    """Classe décrivant un groupe de fichiers"""

    def __init__(self, data):
        # Paramètres obligatoires
        self.name = data.get('name')
        if not self.name:
            raise ValueError("Le paramètre 'name' est obligatoire dans"
                             " une section 'host'.")
        # Paramètres optionnels
        self.files = data.get('files', [])

    def __str__(self):
        output = f"{self.name}: "
        for file in self.files:
            output += file + ", "
        return re.sub(r", $", "", output)

    def get(self, param):
        return getattr(self, param, None)


class CollectorConfig:
    """Classe décrivant la configuration du collecteur"""

    def __init__(self, data):
        # Paramètres optionnels
        self.home = data.get('home', "/var/rotated_files_collector")
        try:
            os.makedirs(self.home)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise ValueError(f"Le répertoire home {self.home} ne peut pas "
                                 "être créé.")
        self.log_level = data.get('log_level', "info")
        self.final_files_owner = data.get('final_files_owner', "root")
        self.final_files_group = data.get('final_files_group', "root")
        self.final_files_mode = str(data.get('final_files_mode', "644"))
        self.expect_timeout = data.get('expect_timeout', 30)
        self.expect_prompt = data.get('expect_prompt', "sftp> ")
        self.sftp_user = data.get('sftp_user')
        self.sftp_id_file = data.get('sftp_id_file')
        self.sftp_password = data.get('sftp_password')
        self.retention_duration = data.get(
            'retention_duration', 60.0 * 60 * 24 * 90)

    def __str__(self):
        output = ""
        for key, value in self.__dict__.items():
            output += str(value) + f" ({key}), "
        return re.sub(r", $", "", output)

    def get(self, param):
        return getattr(self, param, None)


class CollectorHost:
    """Classe décrivant un host"""

    def __init__(self, data, daemon):
        self.daemon = daemon
        config = self.daemon.config
        known_file_groups = self.daemon.file_groups
        self.hashes = []
        # Paramètres obligatoires avant héritage de la conf globale
        self.name = data.get('name')
        if not self.name:
            raise ValueError("Le paramètre 'name' est obligatoire dans"
                             " une section 'host'.")
        # Paramètres optionnels
        self.files = data.get('files', [])
        if 'file_groups' in data:
            for file_group in data.get('file_groups'):
                for known_file_group in known_file_groups:
                    if file_group == known_file_group.name:
                        self.files.extend(known_file_group.files)
        self.data_dir = data.get('data_dir',
                                 f"{config.home}/{self.name}/data")
        self.in_dir = data.get('in_dir',
                               f"{config.home}/{self.name}/in")
        self.out_dir = data.get('out_dir',
                                f"{config.home}/{self.name}/out")
        try:
            os.makedirs(self.data_dir)
            os.makedirs(self.in_dir)
            os.makedirs(self.out_dir)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise ValueError(f"Le répertoire {self.in_dir} ou "
                                 f"{self.out_dir} ne peut pas être créé.")
        self.expect_timeout = data.get('expect_timeout',
                                       config.expect_timeout)
        self.expect_prompt = data.get('expect_prompt',
                                      config.expect_prompt)
        self.sftp_user = data.get('sftp_user', config.sftp_user)
        self.sftp_id_file = data.get('sftp_id_file', config.sftp_id_file)
        self.sftp_password = data.get('sftp_password',
                                      config.sftp_password)
        self.retention_duration = data.get('retention_duration',
                                           config.retention_duration)
        # Paramètres obligatoires après héritage de la conf globale
        if not self.sftp_user:
            raise ValueError(f"Le paramètre 'sftp_user' n'a pas pu être "
                             f"déterminé pour le 'host' {self.name}.")
        if not (self.sftp_id_file or self.sftp_password):
            raise ValueError(f"Aucun des paramètres 'sftp_id_file' ou "
                             f"'sftp_password' n'a pu être déterminé pour le 'host' "
                             f"{self.name}.")
        self.test()
        self.load_hashes()

    def __str__(self):
        output = f"{self.name}: "
        for file in self.files:
            output += file + " (file), "
        for key, value in self.__dict__.items():
            if not key in ('name', 'files'):
                output += str(value) + f" ({key}), "
        return re.sub(r", $", "", output)

    def clean_in_dir(self):
        for file_name in os.listdir(self.in_dir):
            file_path = os.path.join(self.in_dir, file_name)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)

    def collect(self):
        prev_dir = os.getcwd()
        os.chdir(self.in_dir)
        if self.sftp_id_file:
            child = pexpect.spawn(f"sftp -i {self.sftp_id_file} "
                                  f"{self.sftp_user}@{self.name}",
                                  timeout=self.expect_timeout)
            index = child.expect(
                ['Are you sure you want to continue connecting (yes/no)?',
                 'sftp> '])
            if index == 0:
                child.sendline("yes")
                child.expect('sftp> ')
        else:
            child = pexpect.spawn(f"sftp "
                                  f"{self.sftp_user}@{self.name}",
                                  timeout=self.expect_timeout)
            index = child.expect(
                ['Are you sure you want to continue connecting (yes/no)?',
                 'password: '])
            if index == 0:
                child.sendline("yes")
                index = child.expect(['password: ', 'sftp> '])
                if index == 0:
                    child.sendline(f"{self.sftp_password}")
                    child.expect('sftp> ')
            else:
                child.sendline(f"{self.sftp_password}")
                child.expect('sftp> ')
        for file in self.files:
            child.sendline(f"mget {file}*")
            child.expect('sftp> ')
        child.close()
        os.chdir(prev_dir)

    def consolidate(self):
        for radical in (os.path.basename(f) for f in self.files):
            # It's very important not to consolidate current file.
            # So we need to check that file name ends with a number.
            mytmpreg = re.escape(radical) + r"\.(\d+)$"
            myreg = re.compile(mytmpreg)
            for file_name in os.listdir(self.in_dir):
                if myreg.match(file_name):
                    in_file_path = os.path.join(self.in_dir, file_name)
                    sha1 = hashlib.sha1()
                    with open(in_file_path, 'rb') as f:
                        while True:
                            data = f.read(65536)
                            if not data:
                                break
                            sha1.update(data)
                    tmpsha = sha1.hexdigest()
                    if self.is_new_hash(tmpsha):
                        now = datetime.now()
                        nowtt = datetime.timestamp(now)
                        self.hashes.append(
                            {'sha1': tmpsha, 'timestamp': nowtt})
                        self.daemon.log("{} : nouveau fichier {}".format(
                            self.name, file_name), logging.INFO)
                        self.daemon.log("{} : nouveau out index".format(
                            self.get_unused_out_index(radical)), logging.INFO)
                        out_file_path = os.path.join(
                            self.out_dir, radical + '.' + self.get_unused_out_index(radical))
                        os.rename(in_file_path, out_file_path)
                        os.chmod(out_file_path, 0o644)
        self.remove_outdated_hashes()
        self.dump_hashes()

    def dump_hashes(self):
        with open(self.data_dir + '/hashes.yaml', 'w') as outfile:
            yaml.dump(self.hashes, outfile, default_flow_style=False)

    def get(self, param):
        return getattr(self, param, None)

    def get_unused_out_index(self, radical):
        existing_indices = []
        mytmpreg = re.escape(radical) + r"\.(\d{4})$"
        myreg = re.compile(mytmpreg)
        for file_name in os.listdir(self.out_dir):
            if myreg.match(file_name):
                existing_indices.append(str(myreg.match(file_name).group(1)))
        for idx in (f'{i:04}' for i in range(10000)):
            if not idx in existing_indices:
                return idx
        return ''

    def load_hashes(self):
        if os.path.isfile(self.data_dir + '/hashes.yaml'):
            with open(self.data_dir + '/hashes.yaml', 'r') as stream:
                tmp_hashes = yaml.safe_load(stream)
                if tmp_hashes:
                    self.hashes = tmp_hashes

    def is_new_hash(self, myhash):
        for tmphash in self.hashes:
            if tmphash['sha1'] == myhash:
                return 0
        return 1

    def remove_outdated_hashes(self):
        valid_hashes = []
        now = datetime.now()
        nowtt = datetime.timestamp(now)
        for tmp_hash in self.hashes:
            delta = nowtt - tmp_hash['timestamp']
            if delta < self.retention_duration:
                valid_hashes.append(tmp_hash)
        self.hashes = valid_hashes

    def test(self):
        if self.sftp_id_file:
            child = pexpect.spawn(f"sftp -i {self.sftp_id_file} "
                                  f"{self.sftp_user}@{self.name}",
                                  timeout=self.expect_timeout)
            index = child.expect(
                ['Are you sure you want to continue connecting (yes/no)?',
                 'sftp> '])
            if index == 0:
                child.sendline("yes")
                child.expect('sftp> ')
        else:
            child = pexpect.spawn(f"sftp "
                                  f"{self.sftp_user}@{self.name}",
                                  timeout=self.expect_timeout)
            index = child.expect(
                ['Are you sure you want to continue connecting (yes/no)?',
                 'password: '])
            if index == 0:
                child.sendline("yes")
                index = child.expect(['password: ', 'sftp> '])
                if index == 0:
                    child.sendline(f"{self.sftp_password}")
                    child.expect('sftp> ')
            else:
                child.sendline(f"{self.sftp_password}")
                child.expect('sftp> ')
        child.close()
        self.daemon.log(f"Connection avec {self.name} opérationnelle",
                        logging.INFO)


class CollectorDaemon:
    """Classe qui implémente le daemon de collecte."""

    CONFIG_FILE = "/usr/local/etc/file_collector.yaml"
    DEFAULT_LOG_LEVEL = logging.INFO

    def __init__(self):
        signal.signal(signal.SIGHUP, self.handle_sighup)
        signal.signal(signal.SIGINT, self.handle_sigint)
        signal.signal(signal.SIGTERM, self.handle_sigterm)
        self.pid = os.getpid()
        self.logger = None
        self.config = None
        self.file_groups = []
        self.hosts = []
        self.log('Démarrage du collecteur en cours...')
        self.read_config()
        self.log(f'Collecteur démarré pid {self.pid}.')
        self.run()

    def handle_sighup(self, signum, frame):
        """
        Handler pour le signal SIGHUP (1): rechargement de la configuration.
        """
        self.read_config()

    def handle_sigint(self, signum, frame):
        """Handler pour le signal SIGINT (2): arrêt normal du collecteur."""
        self.normal_exit()

    def handle_sigterm(self, signum, frame):
        """Handler pour le signal SIGTERM (15): arrêt normal du collecteur."""
        self.normal_exit()

    def log(self, msg, level=logging.INFO):
        """Gère le système de log"""
        if not self.logger:
            self.logger = logging.getLogger()
            self.logger.addHandler(SysLogHandler('/dev/log', "daemon"))
            self.logger.setLevel(self.DEFAULT_LOG_LEVEL)
        if (level == logging.DEBUG):
            logging.debug(f"DEBUG {msg}")
        elif (level == logging.INFO):
            logging.info(f"INFO {msg}")
        elif (level == logging.WARNING):
            logging.warning(f"WARNING {msg}")
        elif (level == logging.ERROR):
            logging.error(f"ERROR {msg}")

    def normal_exit(self):
        """Gestion de l'arrêt normal du collecteur."""
        self.log('Arrêt du collecteur en cours...')
        self.log('Collecteur arrêté.')
        sys.exit(0)

    def parse_file_group(self, conf):
        """Parse config pour instancier des CollectorFileGroup"""
        for data in conf:
            if data["type"] == "file_group":
                self.file_groups.append(CollectorFileGroup(data))

    def parse_config(self, conf):
        """Parse config pour instancier une CollectorConfig"""
        for data in conf:
            if data["type"] == "global":
                self.config = CollectorConfig(data)
        if not self.config:
            raise ValueError("La section 'global' est obligatoire")

    def parse_host(self, conf):
        """Parse config pour instancier des CollectorHost"""
        for data in conf:
            if data["type"] == "host":
                self.hosts.append(CollectorHost(data, self))

    def read_config(self):
        """Lit la configuration"""
        self.config = None
        self.file_groups = []
        self.hosts = []
        if not os.path.isfile(self.CONFIG_FILE):
            self.log(f"Le fichier de configuration {self.CONFIG_FILE} "
                     "n'existe pas.", logging.ERROR)
            sys.exit(2)
        with open(self.CONFIG_FILE, 'r') as stream:
            self.log(f'Chargement de la configuration {self.CONFIG_FILE}')
            try:
                confobj = yaml.safe_load(stream)
                self.parse_config(confobj)
                self.parse_file_group(confobj)
                self.parse_host(confobj)
            except yaml.YAMLError as exc:
                self.log(exc, logging.ERROR)
                sys.exit(3)
            except ValueError as exc:
                self.log(exc, logging.ERROR)
                sys.exit(3)
            except pexpect.exceptions.TIMEOUT as exc:
                self.log(exc, logging.ERROR)
                sys.exit(3)
            except pexpect.exceptions.EOF as exc:
                self.log(exc, logging.ERROR)
                sys.exit(3)

    def run(self):
        """Boucle principale exécutée par le collecteur"""
        while 1:
            for host in self.hosts:
                host.clean_in_dir()
                host.collect()
                host.consolidate()
            time.sleep(20)


if __name__ == '__main__':
    collector = CollectorDaemon()
