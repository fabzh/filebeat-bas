#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
L'objectif de ce script d'installer le collecteur de fichiers
"""

import getopt
import os
import sys


def main():
    playbook_path = os.path.dirname(os.path.realpath(__file__))
    playbook_path += "/../ansible/install_playbook.yaml"
    cmd = f"ansible-playbook --connection=local --inventory 127.0.0.1, "
    cmd += f"--limit 127.0.0.1 {playbook_path}"
    try:
        opts, args = getopt.getopt(sys.argv[1:], "f", ["force"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(1)
    for o, a in opts:
        if o in ("-f", "--force"):
            cmd += ' -e "force=yes"'
    os.system(cmd)


if __name__ == '__main__':
    main()
