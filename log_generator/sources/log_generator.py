#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
L'objectif de ce script est de générer des messages rsyslog avec la facility local0.
"""

from logging.handlers import SysLogHandler
import logging
from pythonping import ping

logger = logging.getLogger()
logger.addHandler(SysLogHandler('/dev/log', "local0"))

for ip in (
        '1.1.1.1',
        '8.8.4.4',
        '8.8.8.8',
        '216.146.35.35',
        '216.146.36.36',
        '156.154.71.1',
        '156.154.70.1',
        '209.244.0.3',
        '209.244.0.4',
        '84.200.69.80',
        '84.200.70.40',
        '8.26.56.26',
        '8.20.247.20',
        '156.154.70.1',
        '156.154.71.1',
        '199.85.127.10',
        '199.85.126.10',
        '81.218.119.11',
        '209.88.198.133',
        '195.46.39.39',
        '195.46.39.40',
        '107.150.40.234',
        '50.116.23.211',
        '208.76.50.50',
        '208.76.51.51'):
    response_list = ping(ip)
    logging.warn("PING {} min={} max={} avg={}".format(
        ip, response_list.rtt_min_ms, response_list.rtt_max_ms, response_list.rtt_avg_ms))
